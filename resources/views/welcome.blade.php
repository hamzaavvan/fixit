@extends('master')
@section('title', 'Home')


@section('content')
    <div class="flex-center position-ref full-height">
            
        <div class="content">
            <div class="title m-b-md">
                Fixit
            </div>

            <div class="links">
                <a href="https://github.com/hamzaavvan/fixit">GitHub</a>
            </div>
        </div>
    </div>
@endsection