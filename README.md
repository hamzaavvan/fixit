# fixit

App which would surely help

### Features
1) Open an issue/bug
2) Mark as fixed
3) Search for bugs from saved record
4) Delete bugs
5) Edit bugs

### Now Supports `Markdown`
![Fixit Markup Support](https://github.com/hamzaavvan/fixit/blob/master/screenshots/fixit%20markdown%20support%20showdown.gif?raw=true)
